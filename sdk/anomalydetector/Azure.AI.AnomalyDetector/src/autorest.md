# Generated code configuration

Run `dotnet msbuild /t:GenerateCode` to generate code.

``` yaml
input-file:
    -  https://github.com/Azure/azure-rest-api-specs/blob/eef523d27bdd73faaf9d81cd20e272f5083d4e53/specification/cognitiveservices/data-plane/AnomalyDetector/preview/v1.0/AnomalyDetector.json
namespace: Azure.AI.AnomalyDetector
public-clients: true
```
